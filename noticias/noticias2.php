<?php
session_start();

$caminho = '../';

$pagina = "Notícias";
$post   = $_SESSION["post2"];
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
    include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
<section class="noticias corpo-noticias">
    <div class="row">
        <h1><?php echo $post; ?></h1>
    </div>

    <!-- Inicio do conteúdo do post -->
    <div class="container">
        <div class="col-md-9 conteudo">
            <div class="date"><small><i class="fa fa-bookmark" aria-hidden="true"></i> Alberto Aguiar <i class="fa fa-calendar" aria-hidden="true"></i> terça-feira, 6 de junho de 2016</small></div>
            <p><?php echo dah_lipsum(); ?></p>
            <p><?php echo dah_lipsum(); ?></p>
            <p><?php echo dah_lipsum(); ?></p>
            <p><?php echo dah_lipsum(); ?></p>
            <div class="imagem-noticias center-block">
                <img src="<?php echo $caminho;?>imgs/imgs-noticias/noticias2.png" class="img-responsive" alt="Fundo azul com o logo do windows e o pinguin do linux sentando no logo do windows" title="Mistura de Windows com Linux">
            </div>
        </div>

        <!-- Sidebar de noticias -->
        <aside class="col-md-3 sidebar">
            <?php include "sidebar.php"; ?>
        </aside>
        <!-- Término do sidebar de noticias -->

    </div>
    <!-- Término do conteúdo do post -->
</section>
<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
    include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
    include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
