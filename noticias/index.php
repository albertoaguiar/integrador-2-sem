<?php
session_start();

	$caminho = '../';

	//POST 1
	$_SESSION["post1"] = "Ubuntu 16.04, a nova versão do Ubuntu!";

	//POST 2
	$_SESSION["post2"] = "Microsoft leva Ubuntu ao Windows 10!";

	//POST 3
	$_SESSION["post3"] = "Linux 4.6 é lançado e Linus Torvalds fala sobre:";

	//POST 3
	$_SESSION["post4"] = "DuZeru Linux 2.2 - Evolução da distro brasileira:";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Notícias";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
?>

<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
<section class="noticias">
	<div class="row">
		<h1>Notícias</h1>
	</div>

	<div class="container">
		<!-- Inicio dos posts de notícias -->
		<section class="post-noticias">
			<div class="row visao-post">
				<div class="col-md-4">
					<a href="noticias1.php"><img class="img-responsive center-block" src="<?php echo $caminho;?>imgs/imgs-noticias/noticias1.jpg" alt="Post 1"></a>
				</div>
				<div class="col-md-8">
					<h2><?php echo $_SESSION["post1"]; ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, obcaecati, similique. Est similique atque aut itaque minus a, fugiat sit, velit doloribus sed laudantium iure, quidem eveniet molestiae sint provident.<a href="noticias1.php"> Saiba mais...</a></p>
				</div>
			</div>

			<div class="row visao-post">
				<div class="col-md-4">
					<a href="noticias2.php"><img class="img-responsive center-block" src="<?php echo $caminho;?>imgs/imgs-noticias/noticias2.png" alt="Post 2"></a>
				</div>
				<div class="col-md-8">
					<h2><?php echo $_SESSION["post2"]; ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, obcaecati, similique. Est similique atque aut itaque minus a, fugiat sit, velit doloribus sed laudantium iure, quidem eveniet molestiae sint provident.</p>  <a href="noticias2.php"> Saiba mais...</a></p>
				</div>
			</div>

			<div class="row visao-post"><?php $testando = 'testando-oi3'; ?>
				<div class="col-md-4">
					<a href="noticias3.php"><img class="img-responsive center-block" src="<?php echo $caminho;?>imgs/imgs-noticias/noticias3.jpg" alt="Post 3"></a>
				</div>
				<div class="col-md-8">
					<h2><?php echo $_SESSION["post3"]; ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, obcaecati, similique. Est similique atque aut itaque minus a, fugiat sit, velit doloribus sed laudantium iure, quidem eveniet molestiae sint provident.</p>  <a href="noticias3.php">Saiba mais...</a></p>
				</div>
			</div>

			<div class="row visao-post"><?php $testando = 'testando-oi4'; ?>
				<div class="col-md-4">
					<a href="noticias4.php"><img class="img-responsive center-block" src="<?php echo $caminho;?>imgs/imgs-noticias/noticias4.png" alt="Post 4"></a>
				</div>
				<div class="col-md-8">
					<h2><?php echo $_SESSION["post4"]; ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, obcaecati, similique. Est similique atque aut itaque minus a, fugiat sit, velit doloribus sed laudantium iure, quidem eveniet molestiae sint provident.</p>  <a href="noticias4.php">Saiba mais...</a></p>
				</div>
			</div>
		</section>
		<!-- Término dos posts de notícias -->
	</div>
</section>
<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
