<?php if ($pagina == "Inicio" || $pagina == "Quem somos" || $pagina == "Login"):?>
<header class="cabecalho">
	<img class="img-responsive center-block" src="<?php echo $caminho?>imgs/logo.png" alt="Logo Portal L.I.N.U.X com lindos pinguins ao lado da frase PORTAL LINUX" title="Logo versão grande do Portal .L.I.N.U.X">
</header>
<?php else: ?>
<header class="container cabecalho2">
    <div class="col-md-4 col-sm-4">
        <img class="img-responsive center-block logo-pequeno" src="<?php echo $caminho;?>imgs/logo-pequeno.png" alt="Logo Portal L.I.N.U.X com lindos pinguins em cima da frase PORTAL LINUX" title="Logo versão reduzida do Portal L.I.N.U.X">
    </div>

    <div class="col-md-8 col-sm-8 text-right">
        <a href="https://facebook.com" target="_blank"><span class="fa fa-facebook"></span></a>
        <a href="https://twitter.com" target="_blank"><span class="fa fa-twitter"></span></a>
        <a href="https://googleplus.com" target="_blank"><span class="fa fa-google-plus"></span></a>
        <a href="https://linkedin.com" target="_blank"><span class="fa fa-linkedin"></span></a>
        <a href="mailto:contato@portallinux.com.br">contato@portallinux.com.br</a>
    </div>
</header>
<?php endif; ?>
<nav class="navbar">
    <div class="container navbarDefault">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="color: #111 !important;">
            	<span class="sr-only">Toggle navigation</span>
            	<i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
            	<?php if ($pagina == 'Inicio') { ?><li><a class="inicio ativo-inicio" href="<?php echo $caminho;?>index.php">Inicio</a></li>
                <?php } else { ?><li><a class="inicio" href="<?php echo $caminho;?>index.php">Inicio</a></li><?php } ?>

                <?php if ($pagina == 'Notícias') { ?><li><a class="noticias ativo-noticias" href="<?php echo $caminho;?>noticias/">Notícias</a></li>
                <?php } else { ?><li><a class="noticias" href="<?php echo $caminho;?>noticias/">Notícias</a></li><?php } ?>

                <?php if ($pagina == 'Tutoriais') { ?><li><a class="tutoriais ativo-tutoriais" href="<?php echo $caminho;?>tutoriais/">Tutorias</a></li>
                <?php } else { ?><li><a class="tutoriais" href="<?php echo $caminho;?>tutoriais/">Tutorias</a></li><?php } ?>

                <?php if ($pagina == 'Aprenda') { ?><li><a class="aprenda ativo-aprenda" href="<?php echo $caminho;?>aprenda/">Aprenda</a></li>
                <?php } else { ?><li><a class="aprenda" href="<?php echo $caminho;?>aprenda/">Aprenda</a></li><?php } ?>

                <?php if ($pagina == 'Dicas & Segurança') { ?><li><a class="dicas-seguranca ativo-dicas" href="<?php echo $caminho;?>dicas-seguranca/">Dicas & Segurança</a></li>
                <?php } else { ?><li><a class="dicas-seguranca" href="<?php echo $caminho;?>dicas-seguranca/">Dicas & Segurança</a></li><?php } ?>
                <?php if ($pagina == 'Distros') { ?><li><a class="distro ativo-distro" href="<?php echo $caminho;?>distribuicoes/">Distro</a></li>
                <?php } else { ?><li><a class="distro" href="<?php echo $caminho;?>distribuicoes/">Distro</a></li><?php } ?>

                <?php if ($pagina == 'Quem somos') { ?><li><a class="quem-somos ativo-somos" href="<?php echo $caminho;?>quem-somos">Quem Somos</a></li>
                <?php } else { ?><li><a class="quem-somos" href="<?php echo $caminho;?>quem-somos">Quem Somos</a></li><?php } ?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a class="login" href="<?php echo $caminho;?>login/">Login <span class="glyphicon glyphicon-log-in"></span></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>