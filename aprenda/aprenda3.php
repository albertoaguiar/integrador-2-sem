<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
          include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>
<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">
    <section>
        <div class="row">
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda3.png" alt="imagem de tela de terminal exibindo vírus encontrado dentre os dados verificados">
            </div>
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <h1>Aprenda A Procurar Softwares Maliciosos Em Seu <?php echo dah_distro(); ?></h1>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda3-1.png" alt="imagem do resultado do escaneamento executado no terminal pelo software Lynis">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">

                <p><?php echo dah_lipsum(); ?></p>

                <p><?php echo dah_lipsum(); ?></p>

                <h2>Mãos a Obra</h2>

                <p><?php echo dah_lipsum(); ?></p>

                <h3>~s sudo apt-get install rkhunter</h3>

                <h3>~s sudo apt-get install lynis</h3>

                <h3>~s sudo rkhunter -c</h3>
                <p><?php echo dah_lipsum(30); ?> </p>

                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">[ Rootkit Hunter version 1.3.8 ]</li>
                    <li class="list-group-item list-group-item-success">Checking system commands…</li>
                    <li class="list-group-item list-group-item-success">Performing ‘strings’ command checks</li>
                    <li class="list-group-item list-group-item-success">  Checking ‘strings’ command [ OK ]</li>
                    <li class="list-group-item list-group-item-success">Performing ‘shared libraries’ checks</li>
                    <li class="list-group-item list-group-item-success">  Checking for preloading variables [ None found ]</li>
                    <li class="list-group-item list-group-item-success">  Checking for preloaded libraries [ None found ]</li>
                    <li class="list-group-item list-group-item-success">  Checking LD_LIBRARY_PATH variable [ Not found ]</li>
                    <li class="list-group-item list-group-item-success">Performing file properties checks</li>
                    <li class="list-group-item list-group-item-success">  Checking for prerequisites [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/adduser [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/chroot [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/cron [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/groupadd [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/groupdel [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/groupmod [ OK ]</li>
                    <li class="list-group-item list-group-item-success">  /usr/sbin/grpck [ OK ] </li>
                </ul>
                <p><?php echo dah_lipsum(60); ?></p>

                <h3>~s sudo lynix - check-all</h3>

                <p><?php echo dah_lipsum(80); ?></p>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-info"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-warning"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-danger"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                </ul>
            </div>

         </div>
        <a class="btn btn-primary btn-lg" href="index.php" role="button">Voltar a Aprenda &laquo;</a>
     </section>
</div>
<!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
<!-- Término do conteúdo -->


</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
