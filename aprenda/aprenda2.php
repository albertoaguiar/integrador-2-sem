<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>
<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">
    <section>
        <div class="row">
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2.png" alt="imagem do software CuttleFish selecionando a aplicação a ser iniciada">
            </div>
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <h1>Aprenda a Aumentar Sua Produtividade Automatizando As Suas Tarefas Usando O Aplicativo CuttleFish</h1>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-1.png" alt="imagem do ícone CuttleFish na tela de aplicativos">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?>
                </p>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-2.png" alt="imagem da seleção do tipo de ação a ser executada">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-info"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-warning"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-danger"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                </ul>

                <h2>Utilizando</h2>

                <p><?php echo dah_lipsum(); ?></p>

                <h3>~s sudo add-apt-repository ppa:noneed4anick/cutlefish</h3>

                <h3>~s sudo apt-get update</h3>

                <h3>~s sudo apt-get install cuttlefish</h3>

                <p><?php echo dah_lipsum(); ?></p>

            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-3.png" alt="imagem da tela com as categorias de ações e aplicações">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
                <ol class="list-group">
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-info"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-warning"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-danger"><?php echo dah_lipsum(50); ?></li>
                    <li class="list-group-item list-group-item-success"><?php echo dah_lipsum(50); ?></li>
                </ol>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-4.png" alt="imagem exemplificando como nomear e definir os parâmetros da tarefa dentro do Cuttlefish">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-5.png" alt="imagem da seleção do Cuttlefish definindo o volume da aplicação de áudio">
            </div>
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2-6.png" alt="imagem da aplicação de áudio sendo executada através do Cuttlefish">
            </div>
         </div>
        <a class="btn btn-primary btn-lg" href="index.php" role="button">Voltar a Aprenda &laquo;</a>
     </section>


<!-- Término do conteúdo -->

</div>
    <!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
</div>


<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
