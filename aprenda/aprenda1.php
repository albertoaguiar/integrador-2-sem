<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
    $pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
    include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>

<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">

     <section>
        	<div class="post2 row">
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda1.png" alt="imagem do software Furius ISO Mount selecionando a imagem ISO a ser criada ou checada">
            </div>
            <div class="col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <h1 classs="post-principal">Aprenda Como Criar Uma Imagem ISO no <?php echo dah_distro(); ?></h1>

                <p><?php echo dah_lipsum(); ?></p>

                <h3>~s sudo mkdir /mnt/iso</h3>

                <p><?php echo dah_lipsum(50); ?> </p>

                <h3>~s sudo mount -o loop /caminho/pasta/suaImagem.iso /mnt/iso</h3>

                <p><?php echo dah_lipsum(100); ?> </p>

                <h3>~s sudo mkdir /mnt/iso</h3>

                <p><?php echo dah_lipsum(200); ?></p>

                <h3>~s cd /mnt/iso</h3>
                <h3>~s ls</h3>

                <p><?php echo dah_lipsum(120); ?></p>

            </div>
            <div class=" col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda1-1.png" alt="imagem do navegador de arquivos exibindo uma imagm de nome matu20xa_iso">
            </div>
        </div>
         <a class="btn btn-primary btn-lg" href="index.php" role="button">Voltar a Aprenda &laquo;</a>
     </section>

</div>
    <!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
<!-- Término do conteúdo -->

</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
    include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
    include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
