<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
          include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>
<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">
    <section>
        <div class="row">
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4.png" alt="imagem do pinguim símbolo do linux com uma seta verde rodeando-o exemplificando uma restauração de sistema">
            </div>
            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <h1>Aprenda A Restaurar Seu <?php echo dah_distro(); ?> em Caso de Falhas</h1>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4-1.png" alt="imagem da tela inicial do software Systembank utilizado para criar pontos de restauração">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">

                <p><?php echo dah_lipsum(); ?> </p>


            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4-2.png" alt="imagem do software Systemback exibindo a opção de criação de ponto de restauração">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4-3.png" alt="imagem do software Systemback sendo executado">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4-4.png" alt="imagem do software Systemback exibindo a opção de restauração do sistema">
            </div>

            <div class="post-principal col-lg-10 col-md-7 col-sm-12 col-xs-12">
                <p><?php echo dah_lipsum(); ?></p>
            </div>

         </div>
        <a class="btn btn-primary btn-lg" href="index.php" role="button">Voltar a Aprenda &laquo;</a>
     </section>
</div>
<!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
<!-- Término do conteúdo -->


</div>


<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
