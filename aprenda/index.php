<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>
<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">
	<div class="post-principal row">
	    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
			<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda1.png" alt="Fim da instalação do ISO">
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
	        <a class="link-post" href="aprenda1.php">Aprenda Como Criar Uma Imagem ISO no <?php echo dah_distro(); ?></a>
	        <p><?php echo dah_lipsum(); ?> </p><a class="btn btn-primary btn-lg" href="aprenda1.php"><b>Leia Mais...</b></a>
		</div>
	</div>

	<div class="post2 row">
		<div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2.png" alt="Fim da instalação do ISO">
		</div>
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="aprenda2.php">Aprenda a Aumentar Sua Produtividade...</a>
	            <p><?php echo dah_lipsum(); ?></p> <a class="btn btn-primary btn-lg" href="aprenda2.php"><b>Leia Mais...</b></a>
		</div>
	</div>

	<div class="post2 row">
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="aprenda3.php">Aprenda A Procurar Softwares Maliciosos</a>
	            <p><?php echo dah_lipsum(); ?></p> <a class="btn btn-primary btn-lg" href="aprenda3.php"><b>Leia Mais...</b></a>
		</div>
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda3.png" alt="Fim da instalação do ISO">

		</div>
	</div>

	<div class="post3 row">
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4.png" alt="Fim da instalação do ISO">
		</div>
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="aprenda4.php">Aprenda A Restaurar Seu Sistema...</a>
	            <p><?php echo dah_lipsum(); ?></p> <a class="btn btn-primary btn-lg" href="aprenda4.php"><b>Leia Mais...</b></a>
		</div>
    </div>

    <div class="post2 row">
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="aprenda5.php">Aprenda A Acessar uma máquina Ubuntu...</a>
	            <p><?php echo dah_lipsum(); ?></p> <a class="btn btn-primary btn-lg" href="aprenda5.php"><b>Leia Mais...</b></a>
		</div>
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda5.png" alt="Fim da instalação do ISO">
		</div>
	</div>

</div>

<!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
<!-- Término do conteúdo -->
</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
