<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Dicas & Segurança";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
          include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
     <div class="container-fluid">
        <section class="col-md-10 dicas3">
       <div class="dict3 row">
           <h1><b> Apps e serviços inicializados automaticamente</b></h1>


                <div class= " col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<div class="alb">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/alber.jpg" alt="Foto do autor do post com a cabeça inclinada para trás com as narinas abertas e apertando os dentes com força">
                </div>
			    </div>

                <div class= " col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
           <a class="btn btn-info btn-cor-dicas" href="index.php" role="button"><b>Voltar Dicas & Segurança</b></a>
           </div>

           <div class= " col-lg-3 col-md-3 col-sm-3 col-xs-3">
						 <p><?php echo dah_lipsum(); ?></p>
						 <p><?php echo dah_lipsum(); ?></p>
						 <p><?php echo dah_lipsum(); ?></p>
						 <p><?php echo dah_lipsum(); ?></p>
						 <p><?php echo dah_lipsum(); ?></p>
						 <p><?php echo dah_lipsum(); ?></p>
           </div>
                <div class= " col-lg-3 col-md-3 col-sm-3 col-xs-3">

									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>    
            <div class= " col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="alb">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/app.jpg" alt="Pinguim do Linux sentado com um cavalo de tróia ao seu lado saindo vários bichos de dentro do cavalo">
                </div>
            </div>
            </div>

            </div>

</section>
    <aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
           <a class="" href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" alt="logo do site Congelados da Mayra"></a>
         <a href="http://www.congeladosdamayra.com.br" target="_blank">Congelados da Mayra, É só descongelar e apreciar.</a>

           <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" alt="logo do site Sans Super Sonic Prod."></a>
         <a href="http://sansupersonicprod.com/" target="_blank">Artes Gráficas, Padronização e Vídeo.</a>

           <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" alt="logo do site Convelados da Mayra"></a>
         <a href="http://www.arvpaisagismo.eco.br/" target="_blank">Jardins, Paisagismo, Plantio de Grama, etc.</a>

           <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" alt="logo do site Vicky's Perfumes"></a>
         <a href="http://www.vickysperfumes.com.br/" target="_blank">Perfumes Importados e Acessórios.</a>

           <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" alt="logo do site Martins Sociedade AdvogadosConvelados da Mayra"></a>
         <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank">Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</a>

           <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" alt="logo do site Cardiocom"></a>
         <a href="http://cardiocom.com.br/" target="_blank">Clínica Cardiológica Cardiocom.</a>

           <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" alt="logo do site D.H.A. Informática"></a>
         <a href="http://www.dhainformatica.com.br/" target="_blank">Desenvolvimento de Sistemas Personalizados.</a>
        </div>
        </div>
</aside>
     </div>

<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->


 </body>
 </html>
