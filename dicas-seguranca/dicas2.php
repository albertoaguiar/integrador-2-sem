<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Dicas & Segurança";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
          include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
     <div class="container-fluid">

        <section class="col-md-10 dicas2">
       <div class="dict2 row">

           <h1><b> Firewall</b></h1>



                <div class= " col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<div class="dav">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/davi.jpg" alt="Foto do autor do post com gesto de chifres para demonstrar que é rock'n roll">
                </div>
			    </div>

            <div class= " col-lg-8 col-md-8 col-sm-8 col-xs-8">
				<div class="fire">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/firewalll.jpg" alt="Foto mostrando uma rede LAN, a parede do Firewall e depois a ligação até a rede WAN">

                </div>
			    </div>

                <div class= " col-lg-5 col-md-5 col-sm-5 col-xs-5">
									<p><?php echo dah_lipsum(); ?></p>
	                <p><?php echo dah_lipsum(); ?></p>
	                <p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>    
           <a class="btn btn-info btn-cor-dicas" href="index.php" role="button"><b>Voltar Dicas & Segurança</b></a>
           </div>

                <div class= " col-lg-5 col-md-5 col-sm-5 col-xs-5">

									<p><?php echo dah_lipsum(); ?></p>
	                <p><?php echo dah_lipsum(); ?></p>
	                <p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>



                </div>

            </div>
</section>
        <aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
           <a class="" href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" alt="logo do site Congelados da Mayra"></a>
         <a href="http://www.congeladosdamayra.com.br" target="_blank">Congelados da Mayra, É só descongelar e apreciar.</a>

           <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" alt="logo do site Sans Super Sonic Prod."></a>
         <a href="http://sansupersonicprod.com/" target="_blank">Artes Gráficas, Padronização e Vídeo.</a>

           <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" alt="logo do site Convelados da Mayra"></a>
         <a href="http://www.arvpaisagismo.eco.br/" target="_blank">Jardins, Paisagismo, Plantio de Grama, etc.</a>

           <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" alt="logo do site Vicky's Perfumes"></a>
         <a href="http://www.vickysperfumes.com.br/" target="_blank">Perfumes Importados e Acessórios.</a>

           <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" alt="logo do site Martins Sociedade AdvogadosConvelados da Mayra"></a>
         <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank">Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</a>

           <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" alt="logo do site Cardiocom"></a>
         <a href="http://cardiocom.com.br/" target="_blank">Clínica Cardiológica Cardiocom.</a>

           <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" alt="logo do site D.H.A. Informática"></a>
         <a href="http://www.dhainformatica.com.br/" target="_blank">Desenvolvimento de Sistemas Personalizados.</a>
        </div>
        </div>
</aside>
     </div>

<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->


 </body>
 </html>
