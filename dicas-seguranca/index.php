<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Dicas & Segurança";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
  include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->


<!-- Inicio do conteúdo -->
<div class="container">
<section class="col-md-10 dicas">
    <div class="mant1 row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

            <div class="foto">

			<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" alt="Data do post">
            </div>
		</div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <a class="dic1" href="dicas1.php"><b>Manter o Sistema Atualizado</b></a>
            <h6><a href="<?php echo $caminho;?>quem-somos/">by Yuri Sá</a></h6>
	        <p><?php echo dah_lipsum(); ?></p>
            <a class="btn btn-info btn-cor-dicas" href="dicas1.php" role="button"><b>Saiba Mais...</b></a>
		</div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="foto1">
            <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/sistema.jpg" alt="Pinguim do Linux sentado">
            </div>
        </div>
    </div>


	<div class="mant2 row">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" alt="Data do post">
                </div>
                </div>

	       <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

            <a class="dic1" href="dicas2.php"><b>Firewall</b></a>
               <h6><a href="<?php echo $caminho;?>quem-somos/">by Davino Jr.</a></h6>


	            <p><?php echo dah_lipsum(); ?></p>
                <a class="btn btn-info btn-cor-dicas" href="dicas2.php" role="button"><b>Saiba Mais...</b></a>
              </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="foto1">
            <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/apps.jpg" alt="Pinguim do Linux estampando um preservativo">
			</div>
        </div>
    </div>

    <div class="mant3 row">
		        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" alt="Data do post">
                </div>
                </div>


	           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <a class="dic1" href="dicas3.php"><b>Apps e serviços inicializados automaticamente</b></a>
                   <h6><a href="<?php echo $caminho;?>quem-somos/">by Alberto Aguiar</a></h6>


	            <p><?php echo dah_lipsum(); ?></p>
                <a class="btn btn-info btn-cor-dicas" href="dicas3.php" role="button"><b>Saiba Mais...</b></a>
			</div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/linux.jpg" alt="Pinguim do Linux apontando um revólver para o logo do Windows que se encontra com as mãos para cima">
			     </div>
        </div>
        </div>


	<div class="mant4 row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" alt="Data do post">
                </div>
        </div>

			    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

	            <a class="dic1" href="dicas4.php"><b>Conhecimento do Sistema</b></a>
                    <h6><a href="<?php echo $caminho;?>quem-somos/">by Rodrigo Garcia</a></h6>


	            <p><?php echo dah_lipsum(); ?></p>
                <a class="btn btn-info btn-cor-dicas" href="dicas4.php" role="button"><b>Saiba Mais...</b></a>
		        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/know.jpg" alt="Pinguim do Linux olhando através da abertura de uma fechadura">
                </div>
        </div>
    </div>

    <div class="mant5 row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" alt="Data do post">
                </div>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
	            <a class="dic1" href="dicas5.php"><b>Para os geeks...</b></a>
                <h6><a href="<?php echo $caminho;?>quem-somos/">by Alexsander Caldeirão</a></h6>


	            <p><?php echo dah_lipsum(); ?></p> <a class="btn btn-info btn-cor-dicas" href="dicas5.php" role="button"><b>Saiba Mais...</b></a>
		</div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/geek.jpg" alt="Pinguim do Linux de braço cruzado e cara de mau com um estilingue em mãos e logo da Microsoft ao fundo com buracos feitos pelas pedras arremessadas pelo pinguim.">
			      </div>
        </div>
    </div>

        </section>

<!-- Término do conteúdo -->
<!-- Inicio do sidebar -->
<aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
           <a class="" href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" alt="logo do site Congelados da Mayra"></a>
         <a href="http://www.congeladosdamayra.com.br" target="_blank">Congelados da Mayra, É só descongelar e apreciar.</a>

           <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" alt="logo do site Sans Super Sonic Prod."></a>
         <a href="http://sansupersonicprod.com/" target="_blank">Artes Gráficas, Padronização e Vídeo.</a>

           <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" alt="logo do site Convelados da Mayra"></a>
         <a href="http://www.arvpaisagismo.eco.br/" target="_blank">Jardins, Paisagismo, Plantio de Grama, etc.</a>

           <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" alt="logo do site Vicky's Perfumes"></a>
         <a href="http://www.vickysperfumes.com.br/" target="_blank">Perfumes Importados e Acessórios.</a>

           <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" alt="logo do site Martins Sociedade AdvogadosConvelados da Mayra"></a>
         <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank">Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</a>

           <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" alt="logo do site Cardiocom"></a>
         <a href="http://cardiocom.com.br/" target="_blank">Clínica Cardiológica Cardiocom.</a>

           <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" alt="logo do site D.H.A. Informática"></a>
         <a href="http://www.dhainformatica.com.br/" target="_blank">Desenvolvimento de Sistemas Personalizados.</a>
        </div>
        </div>
</aside>
<!-- Término do sidebar -->
</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->


 </body>
 </html>
