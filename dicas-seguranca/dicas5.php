<?php
session_start();

$caminho ="../";
?>

<!DOCTYPE html>
<html>
<?php
	$pagina = "Dicas & Segurança";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
          include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
     <div class="container-fluid">
        <section class="col-md-10 dicas5">
       <div class="dict5 row">
           <h1><b> Para os geeks...</b></h1>


                <div class= " col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<div class="ale">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/alex.jpg" alt="Foto do autor do post fazendo careta com cara de sem vergonha">
                </div>
			    </div>


                <div class= " col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
           </div>
           <div class= " col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<div class="geeks">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/gee.jpg" alt="Imagem ilustrando nerds mais geeks descrevendo a anatomia de cada um e seus interesses e gostos.">



                </div>

           </div>
            </div>
            <h2><b>Conferências mundo Geek</b></h2>
            <div class= " col-lg-2 col-md-2 col-sm-2 col-xs-2">

			    </div>

    <div class= " col-lg-5 col-md-5 col-sm-5 col-xs-5">

			<p><?php echo dah_lipsum(); ?></p>
			<p><?php echo dah_lipsum(); ?></p>
			<p><?php echo dah_lipsum(); ?></p>
           <a class="btn btn-info btn-cor-dicas" href="index.php" role="button"><b>Voltar Dicas & Segurança</b></a>
           </div>

                <div class= " col-lg-5 col-md-5 col-sm-5 col-xs-5">

									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
									<p><?php echo dah_lipsum(); ?></p>
                <div class= " col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<div class="geeks">
                <img class="center-block img-responsive" src="<?php echo $caminho?>imgs/imgs-dicas/geee.jpg" alt="Imagem de o que parece ser uma panela semi tampada e na tampa os dizeres GEEK.">



                </div>

           </div>
                </div>




</section>
    <aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
           <a class="" href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" alt="logo do site Congelados da Mayra"></a>
         <a href="http://www.congeladosdamayra.com.br" target="_blank">Congelados da Mayra, É só descongelar e apreciar.</a>

           <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" alt="logo do site Sans Super Sonic Prod."></a>
         <a href="http://sansupersonicprod.com/" target="_blank">Artes Gráficas, Padronização e Vídeo.</a>

           <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" alt="logo do site Convelados da Mayra"></a>
         <a href="http://www.arvpaisagismo.eco.br/" target="_blank">Jardins, Paisagismo, Plantio de Grama, etc.</a>

           <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" alt="logo do site Vicky's Perfumes"></a>
         <a href="http://www.vickysperfumes.com.br/" target="_blank">Perfumes Importados e Acessórios.</a>

           <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" alt="logo do site Martins Sociedade AdvogadosConvelados da Mayra"></a>
         <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank">Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</a>

           <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" alt="logo do site Cardiocom"></a>
         <a href="http://cardiocom.com.br/" target="_blank">Clínica Cardiológica Cardiocom.</a>

           <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" alt="logo do site D.H.A. Informática"></a>
         <a href="http://www.dhainformatica.com.br/" target="_blank">Desenvolvimento de Sistemas Personalizados.</a>
        </div>
        </div>
</aside>
     </div>
<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->


 </body>
 </html>
