<?php
	$caminho="../";
	$pagina = "Login";
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php"
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php"
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
<div class="container login">
	<h1 class="text-center">Acesso restrito ao usuário</h1>
	<form>
		<div class="form-group">
	    	<label>Usuário</label>
	    	<input type="text" class="form-control" placeholder="email@email.com.br" required>
		</div>
		<div class="form-group">
			<label>Senha</label>
			<input type="password" class="form-control" placeholder="●●●●●" required>
		</div>
	<button type="submit" class="btn btn-default btnLogin">Login</button>
	<div class="esqueceu-senha text-center">
		<a href="http://daray.com.br">Esqueceu a sua senha?</a>
	</div>
	<div class="cadastre-se text-center">
		<a href="http://daray.com.br">Ainda não é usuário? Cadastre-se!</a>
	</div>
	</form>
</div>
<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php"
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php"
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>