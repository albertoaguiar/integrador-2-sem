<?php
    $caminho = "";
    $pagina = "Inicio";
?>

<!DOCTYPE html>
<html>
<?php
    $pagina = "Inicio";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
<div class="container-fluid inicio">
        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/noticias-index.png" alt="Imagem">
            </div>
                <h4 class="text-center titulo-noticias">Microsoft leva Ubuntu ao Windows 10!</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>noticias/noticias2.php" class="btn center-block btn-noticias">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/tutoriais-index.png" alt="Imagem">
            </div>
                <h4 class="text-center titulo-tutoriais">05 distros Linux para usuários iniciantes</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>tutoriais/tutoriais1.php" class="btn center-block btn-tutoriais">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/aprenda-index.png" alt="Imagem">
            </div>
                <h4 class="text-center titulo-aprenda">Aprenda A Procurar Softwares Maliciosos</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>aprenda/aprenda3.php" class="btn center-block btn-aprenda">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/dicas-index.png" alt="Imagem">
            </div>
                <h4 class="text-center titulo-dicas-seguranca">Como manter o seu Sistema Atualizado</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>dicas-seguranca/dicas1.php" class="btn center-block btn-dicas-seguranca">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="imgs/distros-index.png" alt="Imagem">
            </div>
                <h4 class="text-center titulo-distro">Distribuições</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>distribuicoes/" class="btn center-block btn-distro">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="imgs/quemsomos-index.png" alt="Imagem">
            </div>
                <h4 class="text-center">Quem Somos</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam dolores nam id eveniet doloremque sint! Architecto alias ut amet temporibus, quae minima hic dolor, officiis consectetur aliquam rerum, ea reiciendis doloremque.</p>
                <a href="<?php echo $caminho; ?>quem-somos/" class="btn center-block btn-primary">Leia mais</a>
            </div>
        </section>
</div>
<!-- Término do conteúdo -->
<!-- Newsletter -->
<div class="container inicio">
    <section class="newsletter">
        <div class="body-newsletter">
            <h2>Junte-se a nós!</h2>
            <p>Se inscreva e ganhe o convite para a sua área restrita!</p>

            <form action="http://daray.com.br" method="post">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </span>
                    <input class="form-control" type="email" placeholder="seu@email.com" required>
                </div>
                <input type="submit" value="Inscrever-se" class="btn btn-large btn-primary" />
            </form>
        </div>
    </section>
</div>
<!-- Término da newsletter -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
