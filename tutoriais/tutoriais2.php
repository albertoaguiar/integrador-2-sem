<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>
   <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
        <div class="col-md-12">
         <article>
            <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/canonical-ubuntu-logo-1.jpg" class="img-responsive" alt="Logotipo do Ubuntu" title="Logotipo do Ubuntu">
            <div class="col-md-12">
              <h2  class="color-tutoriais"><strong>Como instalar o Ubuntu 14.04 corretamente</strong></h2><br>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" width="765" height="464" src="https://www.youtube.com/embed/ShH2U4D5tjM" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-12">
                      <p class="text-right"><em>Video do Canal <mark>RBtech</mark></em></p>
                  </div>
             </div>
          </article>
    <!--FIM DO POST-->
    <!--INICIO QUEM EU SOU-->
         <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>
    <!--INICIO DO SIDEBAR-->
    <?php
      include "include_sidebar.php";
    ?>
    <!--FIM DO SIDEBAR-->

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
