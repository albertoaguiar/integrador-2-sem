<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<!DOCTYPE html>
<html>
<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>

    <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
    <!---->
        <div class="col-md-12">
         <article>

            <div class="col-md-12">

              <h2 class="color-tutoriais"><strong>Como instalar o RedHat</strong></h2><br>
              <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat.jpg" class="img-responsive" alt="Imagem mostrando o Logotipo do Sistema Operacional RedHat e parte do seu funcionamento" title="RedHat">

              <h2 class="color-tutoriais"><strong>1º PASSO</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install01.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install01.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="1º PASSO"></a>

              <h2 class="color-tutoriais"><strong>2º PASSO</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install02.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install02.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="2º PASSO"></a>

              <h2 class="color-tutoriais"><strong>3º PASSO</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install03.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install03.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="3º PASSO"></a>
              <h2 class="color-tutoriais"><strong>4º PASSO</strong></h2>

              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install04.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install04.png" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="4º PASSO"></a>
              <h2 class="color-tutoriais"><strong>5º PASSO</strong></h2>

              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install05.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install05.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="5º PASSO"></a><br>
              <h2 class="color-tutoriais"><strong>6º PASSO</strong></h2>

              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install06.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install06.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="6º PASSO"></a><br>

              </div>
          </article>

         <!--INICIO QUEM EU SOU-->
         <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>
    <?php
      include "include_sidebar.php";
    ?>

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
