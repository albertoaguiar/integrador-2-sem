<div class="jumbotron">
      <div class="container">
       <div class="col-md-3">
           <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/tutorial.jpg" class="img-responsive img-thumbnail hidden-xs hidden-sm" alt="Imagem ilustrando Tutorial, mostrando um livro e uma lâmpada" title="Tutoriais">
       </div>
       <div class="col-md-9">
        <header>
        <h1 class="color-tutoriais"><strong>TUTORIAIS</strong></h1>
        <p class="text-justify"><?php echo dah_lipsum(); ?></p>
        <p><a class="btn btn-warning btn-lg btn-block btn-cor-tutoriais" href="index.php" role="button">Voltar &laquo;</a></p>
        </header>
        </div>
      </div>
  </div>
