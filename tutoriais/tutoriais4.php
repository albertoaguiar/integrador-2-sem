<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

 <!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
  include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>
   <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
        <div class="col-md-12">
         <article>
            <div class="col-md-12 well well-lg">
                <!--<h2 class="color-tutoriais" style="margin-left: 5px;"><strong>Downloads</strong></h2>-->
                <h3  class="color-tutoriais" style="margin-left: 5px;"><i class="fa fa-lightbulb-o" aria-hidden="true"></i><strong>DICAS</strong></h3>
            </div>
            <div class="col-md-12 media">
              <div class="media-left media-middle">
                <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/debianfordummiesbook.jpg">
                  <img class=" livrodummies media-object" src="<?php echo $caminho;?>imgs/imgs-tutoriais/debianfordummiesbookthumb.jpg" alt="Imagem mostrando a capa do livro Debian for Dummies" title=" Livro Debian for Dummies">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading color-tutoriais">Debian for Dummies</h4>
                <p><?php echo dah_lipsum(); ?></p><br>
                <p><?php echo dah_lipsum(); ?></p>
                <p><a  class="btn btn-default btn-lg color-tutoriais" href="http://www.amazon.com/s/183-2192334-2153748?ie=UTF8&index=books&keywords=0764507133&link_code=qs&tag=bookcrossingc-20" role="button"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Comprar</a></p>
              </div>
              <h2 class="color-tutoriais"><strong>Debian</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/debian-logo.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/debian-logo.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Debian " title="Debian"></a>
            </div>

         </article>
    <!--FIM DO POST-->
    <!--INICIO QUEM EU SOU-->
          <?php
            include "include_quem_sou.php";
          ?>
    <!--FIM QUEM EU SOU-->
         </div>
    </div>
    <!--INICIO DO SIDEBAR-->
    <?php
      include "include_sidebar.php";
    ?>
    <!--FIM DO SIDEBAR-->

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
