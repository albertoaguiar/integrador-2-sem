<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

  <?php
    include "header_tutoriais.php";
  ?>

    <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
    <!---->
        <div class="col-md-12">
         <article>
              <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/linuxforbeginners-695x362.jpg" alt="Logotipo do Linux" title="Logotipo do Linux" class="img-responsive">
                <div class="col-md-12">
              <h2 class="color-tutoriais"><strong>05 distros Linux para usuários iniciantes</strong></h2><br>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>

              <h2 class="color-tutoriais"><strong>01 - UBUNTU</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/ubuntu-695x464.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/ubuntu-695x464.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Ubuntu " title="UBUNTU"></a>

              <h2 class="color-tutoriais"><strong>02 - Pinguy OS</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/pinguyos.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/pinguyos.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional PinguyOS" title="PinguyOS"></a>

              <h2 class="color-tutoriais"><strong>03 - Linux Mint</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Mint.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Mint.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linux Mint " title="Linux Mint"></a>
              <h2 class="color-tutoriais"><strong>04 - Linuxfx</strong></h2>

              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linuxfx.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linuxfx.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linuxfx " title="Linuxfx"></a>
              <h2 class="color-tutoriais"><strong>05 - Linux Deepin</strong></h2>
              <p class="text-justify"><?php echo dah_lipsum(); ?></p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Deepin.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Deepin.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linux Deepin " title="Linux Deepin"></a><br>

              </div>
          </article>
         <!--INICIO QUEM EU SOU-->
          <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>

    <?php
      include "include_sidebar.php";
    ?>

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
