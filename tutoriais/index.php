<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <div class="jumbotron">
      <div class="container">
       <div class="col-md-3">
           <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/tutorial.jpg" class="img-responsive img-thumbnail center-block hidden-xs hidden-sm" alt="Imagem ilustrando Tutorial, mostrando um livro e uma lâmpada" title="Tutoriais">
       </div>
       <div class="col-md-9">
        <header>
        <h1 class="color-tutoriais"><strong>TUTORIAIS</strong></h1>
        <p class="text-justify"><?php echo dah_lipsum(); ?></p>
        </header>
        </div>
      </div>
    </div>


    <!--INICIO DO CONTEUDO ONDE É APRESENTADO QUAIS SÃO OS POSTS-->
    <div class="container">
      <!-- Example row of columns -->
      <div class="col-md-9">
    <!--COLUNA COM AS NOTICAS GRANDES-->
        <div class="col-md-6">
          <article>
          <a href="tutoriais1.php"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/linuxforbeginners-326x170.jpg" alt="Um Pinguim sentado com um sol japonês ao fundo"  class="img-responsive col-sm-12" title="Linux para Iniciantes"></a>
          <div class="col-md-12">
          <h2 class="color-tutoriais"><strong><?php echo dah_distro(); ?> para Iniciantes</strong></h2>
          <p class="text-justify"><?php echo dah_lipsum(100); ?></p>
          <p><a class="btn btn-info btn-cor-tutoriais" href="tutoriais1.php" role="button">Leia mais &raquo;</a></p>
          </div>
          </article>
        </div>
        <div class="col-md-6">
         <article>
             <div class="col-md-12">
                 <a href="tutoriais2.php"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/ubuntu-326x170.jpg" alt="Logotipo do Sistema Operacional Ubuntu em vidro, com seu nome no fundo" class="img-responsive col-sm-12" title="Como Instalar o Ubuntu"></a>
             </div>
             <div class="col-md-12">
              <h2 class="color-tutoriais"><strong>Como Instalar o Ubuntu</strong></h2>
              <p class="text-justify">in elementis mé pra quem é amistosis quis leo. Per aumento de cachacis, eu reclamis. Leite de capivaris, leite de mula manquis. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
              <p><a class="btn btn-info btn-cor-tutoriais" href="tutoriais2.php" role="button">Leia mais &raquo;</a></p>
             </div>
         </article>
       </div>
    <!--TÉRMINO COLUNA COM AS NOTICAS GRANDES-->

    <!--INICIO DAS NOTICIAS PEQUENAS-->


          <div class="col-md-6 col-sm-6 article-baixo">
           <article>
            <div class="col-md-4">
                <a href="tutoriais3.php"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat.png" alt="Imagem em vermelho com o logotipo do Sistema Operacional Red Hat em diagonal ao fundo" title="Como instalar o RedHat" class="img-responsive color-tutoriais" style="margin-top: 15px;"></a>
            </div>
            <div class="col-md-8">
              <h4><a class="color-tutoriais" href="tutoriais3.php">Como instalar o RedHat</a></h4>
              <h5><?php echo dah_lipsum(100); ?></h5>
            </div>
            </article>
          </div>
        <div class="col-md-6 col-sm-6 article-baixo">
           <article>
            <div class="col-md-4">
                <a href="tutoriais4.php"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/debian.jpg" alt="Imagem mostrando o Logotipo do Sistema Operacional Debian" title="Debian para iniciantes" class="img-responsive color-tutoriais" style="margin-top: 15px;"></a>
            </div>
            <div class="col-md-8">
              <h4><a class="color-tutoriais" href="tutoriais4.php">Debian para iniciantes</a></h4>
              <h5><?php echo dah_lipsum(100); ?></h5>
            </div>
        </article>
       </div>
    <!--TÉRMINO DAS NOTICIAS PEQUENAS-->
    </div>

    <?php
      include "include_sidebar.php";
    ?>

    </div> <!-- /container -->
<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
