<div class="img-thumbnail bg-info quem-sou-alex hidden-xs">
  <section class="idpostmaker">
      <div class="col-md-3 col-sm-3 imgavatar">

         <a href="#"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/avatar.jpg" alt="Imagem do autor do Post" title="San Ribeiro" class="img-circle img-responsive"></a>
      </div>
      <div class="col-md-9 col-sm-9">
          <h4 class="text-left"><strong><a href="">QUEM SOU</a></strong></h4>
      </div>
      <div class="col-md-9 col-sm-9 bg-info">
          <h3><small>Alexsander Caldeirão Ribeiro</small></h3>
          <p><small>Estudante do 2º Segundo Semestre de Sistemas para Internet da FATEC São Roque, Design Gráfico, Controlador de Tráfego, Apaixonado pela Interneta!</small></p>
          <!--ICONES DA REDE SOCIAL-->
          <div class="socialicon">
              <a target="new" href="https://www.facebook.com/sansupersonic2001"><i class="fa fa-facebook-square fa-4x" aria-hidden="true"></i></a>
              <a target="new" href="https://twitter.com/sanribeirosonic"><i class="fa fa-twitter-square fa-4x"></i></a>
              <a target="new" href="https://www.linkedin.com/in/alexsanderribeiro"><i class="fa fa-linkedin-square fa-linkedin-square-tuto fa-4x" aria-hidden="true"></i></a>
              <a target="new" href="https://www.youtube.com/user/sansupersonic"><i class="fa fa-youtube-square fa-4x" aria-hidden="true"></i></a>
          </div>
          <!--FIM DOS ICONES DA REDE SOCIAL-->
      </div>
  </section>
</div>