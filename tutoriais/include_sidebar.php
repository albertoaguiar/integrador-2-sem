<!--INICIO DO SIDEBAR-->

            <div class="col-md-3 sidebar-downloads hidden-sm hidden-xs">
                 <aside>
                      <div class="col-md-12">
                        <a href="#"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Botao-Download.png" alt=""></a>
                      </div>
                      <div class="col-md-12">
                       <!--<h2 class="color-tutoriais" style="margin-left: 5px;"><strong>Downloads</strong></h2>-->
                       <h4 class="color-tutoriais" style="margin-left: 5px;"><i class="fa fa-linux" aria-hidden="true"></i>&nbsp;&nbsp;<strong>Escolha sua Distro:</strong></h4>
                          <nav class="nav-downloads">
                            <ul class="color-tutoriais">
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://www.ubuntu.com">Ubuntu</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://www.debian.org/index.pt.html">Debian</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="http://software.opensuse.org/421/pt_BR">openSuse</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://www.centos.org/">CentOS</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="http://www.planetwatt.com/pages/downloads">wattOS</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="http://makululinux.com/">MakuluLinux</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="http://distrowatch.com/table.php?distribution=porteus">Porteus</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://sourceforge.net/projects/pearlinux/?source=navbar">Pear Linux 8</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://elementary.io/">elementary OS Luna</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://manjaro.github.io/download/">Manjaro Linux</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="http://pinguyos.com/download/">PinguyOS 14.04.4-1</a></li>
                                <li><span class="glyphicon glyphicon-download"></span><a target="new" href="https://www.deepin.org/">Linux Deepin 15.1.1</a></li>
                            </ul>
                          </nav>
                      </div>
                  </aside>
                </div>
            <!--FIM DO SIDEBAR-->