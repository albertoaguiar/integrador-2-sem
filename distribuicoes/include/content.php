<?php

function breadcrumb($titulo_breadcrumb)
{
$return_header = '
<section class="distro">
<!-- Cabeçalho -->
                <div class="row hidden-xs">
                    <div class="col-lg-12">

                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-desktop"></i>  <a href="">Distribuições</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> '.urldecode($titulo_breadcrumb).'
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->';

return $return_header;
}


function painel($n_painel = '')
{


  if($_POST["number_distros"]>0) $distros_server_count = $_POST["number_distros"];
  else   $distros_server_count = rand(20,70);               // Esse é o tipo de cuidado que vc não vê por aí Alberto....pega essa função de persistencia.

  if($_POST["number_distros_loja"]>0) $distros_loja_count = $_POST["number_distros_loja"];
  else   $distros_loja_count = rand(3,10);               // Esse é o tipo de cuidado que vc não vê por aí Alberto....pega essa função de persistencia.

  $painel1 = ' panel-blue'; // Fiz essa atrocidade pq no LAMP funcionou bem, mas no bitnami parecia sujar memória, sei lá.... aí fiz desse jeito aqui....foda-se a vida.
  $painel2 = ' panel-green';
  $painel3 = ' panel-yellow';

  if($n_painel == 1) $painel1 = ''; // Na vdd nem é tão feio, em C++ eu faço direto, chama "método exclusivo"
  if($n_painel == 2) $painel2 = '';
  if($n_painel == 3) $painel3 = '';


  $retorno_painel = '

     <div class="row">
                    <a href="#" onClick="document.getElementById(\'#generico\').submit();" class="none">
                      <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                          <div class="panel'.$painel1.'">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-television fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="hidden-xs grande">'.rand(180,600).'</div>
                                          <div class="hidden-xs">Distros Monitoradas</div>
                                      </div>
                                  </div>
                              </div>
                                  <div class="panel-footer">
                                      <div class="pull-left">'.dah_link_generico('distros_monitoradas','Distros','',false).'</div>
                                      <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                      <div class="clearfix"></div>
                                  </div>
                          </div>
                    </div>
                    </a>

                    <a href="#" onClick="document.getElementById(\'#servers\').submit();" class="none">
                      <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                          <div class="panel'.$painel2.'">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-tasks fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="hidden-xs grande">'.$distros_server_count.'</div>
                                          <div class="hidden-xs">Distros de Servidor</div>
                                      </div>
                                  </div>
                              </div>

                                  <div class="panel-footer">
                                      <div class="pull-left hidden-xs">'.dah_link_servers('distros_servers','Server Distros',$distros_server_count,false,true).'</div>
                                      <div class="pull-left hidden-md hidden-lg hidden-sm pull-left">'.dah_link_servers('distros_servers','Servers',$distros_server_count,false,false).'</div>
                                      <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                      <div class="clearfix"></div>
                                  </div>

                          </div>
                      </div>
                    </a>

                    <a href="#" onClick="document.getElementById(\'#loja\').submit();" class="none">
                    <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                        <div class="panel'.$painel3.'">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="hidden-xs grande">'.$distros_loja_count.'</div>
                                        <div class="hidden-xs">Distros comerciais</div>
                                    </div>
                                </div>
                            </div>

                                <div class="panel-footer">
                                    <div class="hidden-xs pull-left">'.dah_link_loja('loja','Lojinha do Brimo',$distros_loja_count,false,true).'</div>
                                    <div class="hidden-md hidden-lg hidden-sm pull-left">'.dah_link_loja('loja','Lojinha',$distros_loja_count,false,false).'</div>
                                    <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                    <div class="clearfix"></div>
                                </div>

                        </div>
                    </div>
                    </a>

                    <a href="../aprenda">
                    <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="hidden-xs grande">'.rand(300,500).'</div>
                                        <div class="hidden-xs">Artigos de ajuda</div>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-footer lbl-ajuda">
                                    <div class="hidden-xs pull-left">Ajuda (portal)</div>
                                      <div class="hidden-md hidden-lg hidden-sm pull-left">Ajuda</div>
                                    <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                    </a>
                </div>
                <!-- /.row -->';

    return $retorno_painel;

}



function aside_tabela($numero)
{
    $retorno_aside .= '<aside>';
    $retorno_aside .= dah_table($numero,2,1);
    $retorno_aside .= '</div></aside>';

    return $retorno_aside;

}

function aside_movimentacao()
{
    $retorno_aside = '
                 <aside>
                    <div class="hidden-sm hidden-xs col-md-4 pull-left">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Movimentação o L.I.N.U.X </h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <div class="list-group-item">
                                        <div class="badge">agora</div>
                                        <i class="fa fa-fw fa-calendar"></i> Atualização do '.dah_link_distro().' remarcada
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(1,20).' minutos atrás</div>
                                        <i class="fa fa-fw fa-comment"></i> Usuário respondeu ao comentário
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(21,30).' minutos atrás</div>
                                        <i class="fa fa-fw fa-truck"></i> '.rand(50000,90000).' DVDs do Ubuntu enviados
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(31,59).' minutos atrás</div>
                                        <i class="fa fa-fw fa-money"></i> '.rand(500,900).' Distros compradas hoje
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">1 hora atrás</div>
                                        <i class="fa fa-fw fa-user"></i>  '.rand(30,50).' Usuários registrados hoje
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(2,23).' horas atrás</div>
                                        <i class="fa fa-fw fa-check"></i> '.rand(30,50).' Commits no portal do RBNS
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">dois dias atrás</div>
                                        <i class="fa fa-fw fa-check"></i> Finalizando o side bar"
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
                                       ';
    return $retorno_aside;

}

function corpo($titulo)
{
    $globals['$post'] = $titulo;

    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8">';
    $retorno_conteudo .= '<img class="img-rounded img-responsive center-block" src="../imgs/imgs-distribuicoes/distros.jpg" alt="distribuicoes">';
    $retorno_conteudo .= '<h2>'.$titulo.'</h2>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '</div>';

    $retorno_conteudo .= aside_tabela(20);
    $retorno_conteudo .= '</div></section>'; // Do container

    //$retorno_conteudo .= '<section class="distro">'; // Da outra função ainda. Para validar bonito.

   return $retorno_conteudo;

}



function corpo_distros($titulo,$tipo_aside)
{
    $globals['$post'] = $titulo;

    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h2>'.$titulo.'</h2>';
    $retorno_conteudo .= '<img class="hidden-xs col-md-6 pull-right img-responsive" src="../imgs/imgs-distribuicoes/minisicha-vertical.png" alt="Grafico de utilização de distribuições Linux">';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '</div>';
    $retorno_conteudo .= aside_movimentacao();
    if($tipo_aside == 1) $retorno_conteudo .= dah_table(25,4,0);
    $retorno_conteudo .= '</div>'; // Do container
    $retorno_conteudo .= '</div></section>';

   return $retorno_conteudo;

}

function corpo_servers($titulo,$tipo_aside)
{
    $number_distros = $_POST["number_distros"];

    $retorno_conteudo .= '<div class="container">';
//    $retorno_conteudo .= '<img class="center-block img-responsive" src="../imgs/imgs-distribuicoes/grafico.png" alt="Grafico de utilização de distribuições Linux">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h2>'.$number_distros.' '.$titulo.' Distros em detalhes!</h2>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<img class="img-responsive" src="../imgs/imgs-distribuicoes/serverfarm.png" alt="Tux! Bitches!">';
    $retorno_conteudo .= '<ol type="1" style="list-style: decimal inside;">';

    $retorno_conteudo .= '<li><div>'.dah_link_distro('',true,false).' - '.dah_lipsum(70).'</div></li>';

        for($i = 2; $i <= $number_distros; $i++)
        {
            $retorno_conteudo .= '<li><div>'.dah_link_distro('',true,false).' - '.dah_lipsum(70).'</div></li>';
        }
    $retorno_conteudo .= '</ol></div>';
    $retorno_conteudo .= aside_tabela($number_distros);
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}

function corpo_loja($titulo,$tipo_aside)
{
    $number_distros = $_POST["number_distros_loja"];
    $array_panels = array("panel-yellow","panel-red","panel-green","panel-blue");
    $array_pay_type = array("credit-card","cc-discover","paypal");
    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h1>'.$titulo.'</h1>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';

    for($i = 1; $i <= $number_distros; $i++)
    {
      $distro_form = dah_distro();
    $color_panel = $array_panels[array_rand($array_panels)];
    $pay_type =    $array_pay_type[array_rand($array_pay_type)];
    $retorno_conteudo .= '

    <a href="# " onClick="document.getElementById(\'#'.$distro_form.'\').submit();">
      <div class="col-xs-6 col-lg-4 col-md-4">
          <div class="panel '.$color_panel.'">
              <div class="panel-heading">
                  <div class="row">
                      <div class="hidden-xs col-sm-3 pull-left">
                          <i class="fa fa-linux fa-3x"></i>
                      </div>
                      <div class="col-xs-3 text-center">
                          <i class="fa fa-shopping-cart fa-4x"></i>
                      </div>
                      <div class="hidden-xs col-sm-3 pull-right">
                          <i class="fa fa-inverse fa-'.$pay_type.' fa-2x"></i>
                      </div>
                  </div>
              </div>


                  <div class="panel-footer">
                      <div>'.dah_link_distro($distro_form,false).'</div>
                      <div class="pull-right"><i class="fa fa-dollar"></i><b>'.rand(10,99).'9,99</b><small>/ano</small></div>
                      <div class="clearfix"></div>
                  </div>

          </div>
      </div>
      </a>';
    }
     $retorno_conteudo .= '
    <i class="fa fa-paypal fa-2x" aria-hidden="true" ></i> - PayPal <br>
    <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i> - Visa + Master <br>
    <i class="fa fa-cc-discover fa-2x" aria-hidden="true"></i> - Visa + Master + Discover Card <br>
    ';

    $retorno_conteudo .= '</div>';

    $retorno_conteudo .= aside_tabela($number_distros);
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}

function corpo_distro_individual($titulo,$tipo_aside)
{
    $globals['$post'] = $titulo;

    $distro = $_POST["distro"];
    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-xs-12 col-md-8 pull-right">';
    $retorno_conteudo .= '<h1>'.$titulo.'</h1>';
    $retorno_conteudo .= '<img class="img-responsive col-xs-12 col-sm-5" src="../imgs/imgs-distribuicoes/tux'.rand(1,7).'.png" alt="Tux! Bitches!">';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';
    $retorno_conteudo .= '</div>';
    $retorno_conteudo .= aside_movimentacao();
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}






?>
